/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

jQuery(function($) {
});

function delete_solr() {
    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        data: {
            action: "delete_solr_documents",
        },
        success: function (data) {
            alert("deleting index finished");
        }
    });
}

function reindex_solr() {
    jQuery.ajax({
        type: "POST",
        url: ajaxurl,
        data: {
	    action: "reindex_solr_documents",
        },
        success: function (data) {
            alert("re-indexing finished");
        }
    });
}
