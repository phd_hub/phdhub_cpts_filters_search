<?php
/*
Plugin Name: PhD Hub CPTs Filters & Search
Plugin URI: http://it.auth.gr
Description: Custom Filters and Search Engine for PhD Hub CPTs
Text Domain: phdhub-fs
Version: 1.0.0
Author: Ioannis Zachros
Author URI: https://it.auth.gr
License: GPLv2 or later
*/

defined('ABSPATH') or die;

define("PHDHUB_CPTS_SEARCH_PLUGIN_DIR", dirname(plugin_basename(__FILE__)));

/*
 * include composer autoload file
 */
require 'vendor/autoload.php';

/*
 * Include custom class
 */

require 'inc/PhDOffer.php';
require 'inc/PhDOfferPosting.php';
require 'inc/CooperationOffer.php';
require 'inc/CooperationOfferPosting.php';


add_action( 'plugins_loaded', 'phdhub_cpts_filters_search_load_textdomain' );
function phdhub_cpts_filters_search_load_textdomain() {
	load_plugin_textdomain( 'phdhub-fs', false, dirname( plugin_basename(__FILE__) ) . '/lang/' );
}

/*
 * Include plugin's main file
 */
include_once dirname( __FILE__ ) . '/inc/config.php';

?>
