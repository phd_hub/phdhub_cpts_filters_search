<?php

/**
 * Description of PhDOffer
 *
 * @author Sotiris Karampatakis Open Knowledge Greece
 *  
 */
use Solarium\QueryType\Update\Query\Document\Document;

class PhDOffer extends Document {    
    
    protected $id, $fields;
        
    public function __construct(array $fields = array(), array $boosts = array(), array $modifiers = array()) {
        $this->id = $fields["id"];                
        $this->setFields();        
        parent::__construct($this->fields, $boosts, $modifiers);
    }
    
    private function setFields(){
        $this->fields = array();	
        $this->setPostData();
        $this->setPostMetaData();
        $this->setPostCategory();
	    $this->setInstitutionInfo();
	    $this->setPostResearchAreas();
	    $this->setPermalink();
	    $this->setMD5ID();
        $this->remove_uwnanted_fields();
        $this->dateFix();
    }
    
    public function remove_uwnanted_fields(){
        $removed_fields = [
            "post_parent",
            "ping_status",
            "comment_status",
            "comment_count",
            "_edit_lock",
            "_edit_last",
            "_the_champ_meta",
            "wppb-content-restrict-type",
            "simplefavorites_count",            
            "_heateor_ss_shares_meta",
            "filter",
            "post_mime_type",
            "menu_order",
            "post_content_filtered",
            "post_password",
	        "post_author",
            "to_ping",
            "post_excerpt",
            "pinged",
            "page_template",
	        "ID",
	        "short_info",
            "post_content",
            "wppb-content-restrict-custom-redirect-url",
            "_wp_old_slug",
            "_wp_old_date",
	        "_wp_trash_meta_status",
	        "_wp_trash_meta_time",
	        "_wp_desired_post_slug",
            "post_views_count",
        ];
        $keys = array_keys($this->fields);
        $unwanted = preg_grep("/wppb|_oembed_/", $keys);
        $to_remove = array_merge($removed_fields, $unwanted);
        $this->fields = $this->unset_multiple($this->fields, $to_remove);
    }

    function unset_multiple(array $array, array $keys){
        foreach($keys as $key){
            unset($array[$key]);
        }
        return $array;
    }

    /*
     * A function to update the fields array with data
     */
    private function updateFields(array $data = []){
        if(!empty($data)){
            $this->fields = array_merge($this->fields, $data);
        }
    }

    /*
     * A function to set the id of the phd offer
     */
    private function setPermalink(){
	    $this->fields["post_permalink"] = get_post_permalink($this->id);
    }

   /*
    * A function to fix the date format in content
    */
    private function dateFix(){
        $toFIX = ["deadline", "starting_date"];
        $toFIX2 = ["post_date", "post_date_gmt", "post_modified", "post_modified_gmt"];
        foreach($toFIX as $field){
		    $this->fields[$field] = $this->dateFormat($this->fields[$field][0]);
	    }
        foreach($toFIX2 as $field){
            $this->fields[$field] = $this->dateFormat($this->fields[$field]);
        }

    }

    private function dateFormat($date){
   	    return date("Y-m-d\TH:i:s\Z", strtotime($date));
    }

    private function createMD5ID(){
	    return md5( $this->fields["guid"] );
    }
    
    private function setMD5ID(){
	    $this->fields["id"] = $this->createMD5ID();
    }

    public function getMD5ID(){
        return $this->fields["id"];
    }
    
    /*
     * A function to set the general post data
     */
    private function setPostData(){        
        $post = get_post($this->id, ARRAY_A);        
        $this->updateFields($post);
    }

    /*
     * A function to set the post meta data
     */
    private function setPostMetaData(){
        $post_meta = get_post_meta($this->id);
        $this->updateFields($post_meta);       
    }
    
    
    /*
     *  select wp_terms.name, wp_termmeta.meta_value from wp_term_relationships  inner join wp_terms ON wp_term_relationships.term_taxonomy_id=wp_terms.term_id inner join wp_term_taxonomy on wp_term_relationships.term_taxonomy_id=wp_term_taxonomy.term_id inner join wp_termmeta ON wp_term_relationships.term_taxonomy_id=wp_termmeta.term_id where wp_term_relationships.object_id=14 AND wp_term_taxonomy.taxonomy="fields-of-science" AND wp_termmeta.meta_key="SKOSTP_IRI";


     */
    /*
     * A function to set the post taxonomy data
     */
    private function setPostCategory(){                        
        $categories["categories_name"] = [];
        $categories["categories_iri"] = [];
        //TODO: to remove this as this as it do not reflect the actual permalink of the respective category term unless in alpha order
        $categories["categories_permalink"] = [];
        $results = $this->getCategoryQuery("fields-of-science");
        foreach ($results as $result){
            array_push($categories["categories_name"], $result->name);
	        array_push($categories["categories_permalink"], get_category_link($result->term_id));
            array_push($categories["categories_iri"], $result->iri);            
        }
        $this->updateFields($categories);
    }
    
    private function getCategoryQuery( $taxonomy, $meta_key = "SKOSTP_IRI", $type = OBJECT ){
        global $wpdb;
        //$blog_id = get_current_blog_id();
        $table_prefix = $wpdb->prefix;
	    $results = $wpdb->get_results(
                'select ' . $table_prefix . 'terms.name, ' . $table_prefix . 'terms.term_id, ' . $table_prefix . 'termmeta.meta_value as iri from ' . $table_prefix . 'term_relationships '
              . 'inner join ' . $table_prefix . 'terms ON ' . $table_prefix . 'term_relationships.term_taxonomy_id=' . $table_prefix . 'terms.term_id '
              . 'inner join ' . $table_prefix . 'term_taxonomy on ' . $table_prefix . 'term_relationships.term_taxonomy_id=' . $table_prefix . 'term_taxonomy.term_id '
              . 'inner join ' . $table_prefix . 'termmeta ON ' . $table_prefix . 'term_relationships.term_taxonomy_id=' . $table_prefix . 'termmeta.term_id '
              . 'where '
              . '' . $table_prefix . 'term_relationships.object_id=' . $this->id
              . ' AND ' . $table_prefix . 'term_taxonomy.taxonomy="' . $taxonomy . '" '
              . ' AND ' . $table_prefix . 'termmeta.meta_key="' . $meta_key . '";'
                , $type);
        return $results;
    }
    
    private function setPostQualifications(){
        
    }
    
    private function setPostResearchAreas(){
        $research_areas["research_areas_name"] = [];
        $research_areas["research_areas_iri"] = [];
        $results = $this->getCategoryQuery("research-areas", "iri");
        foreach ($results as $result){	    
            array_push($research_areas["research_areas_name"], $result->name);
            array_push($research_areas["research_areas_iri"], $result->iri);	    
        }
        $this->updateFields($research_areas);
	    $this->setPostExpandedResearchAreas($results);
    }
    
    private function setInstitutionInfo(){	
        $institution = get_page_by_path( $this->fields["institution"][0], '', 'institutions' );
        $this->fields["institution_permalink"] = get_post_permalink($institution);
        $this->fields["institution_logo"] = esc_attr( get_post_meta( $institution->ID, 'institution_logo', true ) );
        $this->fields["institution_city"] = esc_attr( get_post_meta( $institution->ID, 'city', true ) );
        $this->fields["institution_country"] = esc_attr( get_post_meta( $institution->ID, 'country', true ) );
    }	

    function expandResearchAreas($terms){
        $prefixes = array(
            'skos' => 'http://www.w3.org/2004/02/skos/core#'
        );
        $queryBuilder = new Asparagus\QueryBuilder($prefixes);
        $queryBuilder->selectDistinct("?iri", "?label")
            ->where("?concept", "skos:broader+", "?iri")
            ->where("?iri", "skos:prefLabel", "?label")
            ->filter("langmatches(lang(?label), 'en')")
            ->values(["?concept" => $this->flatten_terms_object($terms, "iri")], true);

        $endpoint = new \EasyRdf_Sparql_Client("http://data.phdhub.eu/sparql");
        $results = $endpoint->query($queryBuilder->getSPARQL());
        return $results;
    }

    function setPostExpandedResearchAreas($terms){
        if(empty($terms)){
            return;
        }
        $research_areas["expanded_research_areas_name"] = [];
        $research_areas["expanded_research_areas_iri"] = [];
        $results = $this->expandResearchAreas($terms);
        foreach ($results as $result){
            array_push($research_areas["expanded_research_areas_name"], $result->label->getValue());
            array_push($research_areas["expanded_research_areas_iri"], $result->iri->getUri());
        }
        $this->updateFields($research_areas);

	
    }
    
    function flatten_terms_object($terms, $key = "iri"){
        $values = [];
        //TODO: duplication of elements is a hotfix with virtuoso error when only one term is given.
        if (count($terms) == 1){
            $term = $terms[0];
            array_push($values, $term->$key, $term->$key);
        }
        else{
            foreach ($terms as $term){
               array_push($values, $term->$key);
            }
	    }
	    return $values;
    }
}
