<?php
/*
 * Search results page template for PhD Openings
 */

defined( 'ABSPATH' ) or die;

get_header();

$config	 = array(
    'endpoint' => array(
	'localhost' => array(
	    'host'	 => SOLR_HOST,
	    'port'	 => 8983,
	    'path'	 => '/solr/' . SOLR_CALLS_CORE . '/'
	)
    )
);
// create a client instance
$client	 = new Solarium\Client( $config );

// get a select query instance based on the config
$query = $client->createSelect( array( 'querydefaultoperator' => 'AND' ) );

if ( isset( $_GET[ 'find_cooperation_offers' ] ) ) {
    $query->setQuery( 'all_text:(' . $s . ')' );
    $resultset = $client->select( $query );
} else if ( isset( $_GET[ 'find_cooperation_offers_advanced' ] ) ) {
    if ( isset( $_GET[ 'field_of_science' ] ) ) {
	$field_of_science = $_GET[ 'field_of_science' ];
    }
    if ( isset( $_GET[ 'region' ] ) ) {
	$region = $_GET[ 'region' ];
    }
    /*
    $master_degree		 = $_GET[ 'master_degree' ];
    $duration_min		 = $_GET[ 'duration_min' ];
    $duration_max		 = $_GET[ 'duration_max' ];
    $type			 = $_GET[ 'type' ];
    $funded			 = $_GET[ 'funded' ];
    $tuition_fees_min	 = $_GET[ 'tuition_fees_min' ];
    $tuition_fees_max	 = $_GET[ 'tuition_fees_max' ];
    $currency		 = $_GET[ 'tuition_fees_currency' ];
    $starting_date		 = $_GET[ 'starting_date' ];
    $deadline		 = $_GET[ 'deadline' ];
    */
    if ( $s != NULL ) {
	$query->setQuery( 'all_text:(' . $s . ')' );
    } else {
	$query->setQuery( '*:*' );
    }
    /*
    if ( $funded != 'all' ) {
	$query->createFilterQuery( 'funded' )->setQuery( 'funded:' . $funded );
    }
    if ( $type != 'all' ) {
	$query->createFilterQuery( 'type' )->setQuery( 'type:' . $type );
    }
    if ( $master_degree != 'all' ) {
	$query->createFilterQuery( 'masterDegree' )->setQuery( 'master_degree:' . $master_degree );
    }
    if ( $duration_min != NULL && $duration_max != NULL ) {
	$query->createFilterQuery( 'duration' )->setQuery( 'duration:[' . $duration_min . ' TO ' . $duration_max . ']' );
    }
    if ( $tuition_fees_min != NULL && $tuition_fees_max != NULL ) {
	$query->createFilterQuery( 'tuitions' )->setQuery( 'tuition_fees:[' . $tuition_fees_min . ' TO ' . $tuition_fees_max . ']' );
    }
    if ( $currency != NULL ) {
	$query->createFilterQuery( 'tuitionsCurrency' )->setQuery( 'tuition_fees_currency:' . $currency );
    }
    if ( $starting_date != NULL ) {
	if ( $starting_date != NULL && $deadline != NULL ) {
	    $query->createFilterQuery( 'startingDate' )->setQuery( 'starting_date:[' . $starting_date . 'T00:00:00Z TO ' . $deadline . 'T00:00:00Z]' );
	} else if ( $starting_date != NULL && $deadline == NULL ) {
	    $query->createFilterQuery( 'startingDate' )->setQuery( 'starting_date:[' . $starting_date . 'T00:00:00Z TO *]' );
	}
    }
    if ( $deadline != NULL ) {
	if ( $starting_date != NULL && $deadline != NULL ) {
	    $query->createFilterQuery( 'deadline' )->setQuery( 'deadline:[' . date( 'Y-m-d', strtotime( $starting_date ) ) . 'T00:00:00Z TO ' . date( 'Y-m-d', strtotime( $deadline ) ) . 'T00:00:00Z]' );
	} else if ( $starting_date == NULL && $deadline != NULL ) {
	    $query->createFilterQuery( 'deadline' )->setQuery( 'deadline:[' . date( 'Y-m-d' ) . 'T00:00:00Z TO ' . date( 'Y-m-d', strtotime( $deadline ) ) . 'T00:00:00Z]' );
	}
    } */
    $resultset = $client->select( $query );
}
?>
<div id="phd-opening-archive-new">
    <div class="container">
        <div class="uk-grid">
            <div class="uk-width-2-3">
                <h2 class="search-results-title">
                    <?php echo __( 'Search results for', 'phdhub-fs' ) . ': ' . $s; ?>
                    <span><?php echo __( 'Results found', 'phdhub-fs' ) . ': ' . $resultset->getNumFound(); ?></span>
                </h2>
                <div id="search-offers">
                    <?php
                        if ( is_active_sidebar( 'cooperation-calls-archive-sidebar' ) ) {
                            dynamic_sidebar( 'cooperation-calls-archive-sidebar' );
                        }
                    ?>
                </div>

                <div class="list-offers uk-grid">
                    <?php
                        foreach ( $resultset as $document ) {    
                            $offer	 = new CooperationOfferPosting( $document );
                    ?>
                    <div class="uk-width-1-1">
                        <div class="uk-grid uk-grid-match">
                            <div class="uk-width-1-5 phd-opening-thumbnail">
                                <?php  
                                    if ( $offer->company_logo ) {
                                ?>
                                <a href="<?php echo $offer->company_permalink; ?>"><img src="<?php echo $offer->company_logo; ?>" alt="Logo"></a>
                                <?php
                                    } else if ( $offer->institution_logo ) {
                                ?>	    
                                <a href="<?php echo $offer->institution_permalink; ?>"><img src="<?php echo $offer->institution_logo; ?>" alt="Logo"></a>
                                <?php
                                    }
                                ?>
                            </div>
                            <div class="uk-width-4-5 phd-opening-details">
                                <h4>
                                    <a href="<?php echo $offer->permalink; ?>"><?php echo $offer->title; ?></a>
                                </h4>
                                <p class="phd-opening-info">
                                    <?php
                                        if ( $offer->company_label ) {
                                    ?>
                                    <a href="<?php echo $offer->company_permalink; ?>">
                                        <i class="fas fa-building"></i> <?php echo $offer->company_label; ?>
                                    </a>
                                    <?php
                                        } else if ( $offer->institution_label ) {
                                    ?>
                                    <a href="<?php echo $offer->institution_permalink; ?>">
                                        <i class="fas fa-university"></i> <?php echo $offer->institution_label; ?>
                                    </a>
                                    <?php
                                        }
                                    ?>
                                    <span class="phd-opening-date">
                                        <i class="fas fa-calendar-alt"></i> <?php echo date_i18n('d M Y', strtotime( $offer->post_date )); ?>
                                    </span>
                                </p>
                            </div>        
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
            <div class="uk-width-1-3">
                <?php get_sidebar(); ?>
            </div>
        </div>
    </div>
</div>
    
<?php
	get_footer();
?>