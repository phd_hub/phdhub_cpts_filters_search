<?php

defined('ABSPATH') or die;

/* 
 * Enqueue CSS & JS Files for PhD Hub CPTs Filters & Search - Frontend
 */
function phdhub_fs_scripts() {
		wp_register_style('jquery-ui', plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/css/jquery-ui.css'));
		wp_enqueue_style('jquery-ui');
		wp_register_style('phdhub-fs-select2', plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/css/select2.min.css'));
		wp_enqueue_style('phdhub-fs-select2');
		wp_register_style('phdhub-fs-style', plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/css/style.css'));
		wp_enqueue_style('phdhub-fs-style');
		wp_enqueue_script('jquery-ui-slider');
		
		wp_enqueue_script( 'jquery-select2', plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/js/select2.min.js'), array('jquery'));
		wp_enqueue_script( 'filters-custom-scripts', plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/js/custom.js'), array('jquery'));

}
add_action( 'wp_enqueue_scripts', 'phdhub_fs_scripts' );
