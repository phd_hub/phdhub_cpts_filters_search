<?php
/*
 * PhD Hub Filters and Search plugin functions
 */

 defined('ABSPATH') or die;


 /*
  * Set Search Results Page for PhD Openings
  */
 function phd_openings_search_template($template) {
	global $wp_query;
	$post_type = get_query_var('post_type');
	if( $wp_query->is_search && $post_type == 'phd-openings' ) {
		$template = dirname(plugin_dir_path(__DIR__)) . '/inc/search-phd-openings.php';
	} else if ( $wp_query->is_search && $post_type == 'cooperation-calls' ) {
		$template = dirname(plugin_dir_path(__DIR__)) . '/inc/search-cooperation-calls.php';
	}
	return $template;
}
add_filter('template_include', 'phd_openings_search_template', 99);

function save_phd_opening_solr( $phd_opening_id, $phd_opening ){
    if ( $phd_opening->post_type != 'phd-openings' ) {
		return;
    }
    if ( $phd_opening->post_status == 'trash' ) {
        //offers are identified by md5 hash of the guid
        delete_phd_opening_solr($phd_opening->guid);
        return;
    }
	//skip draft savings.
    if ( $phd_opening->post_date_gmt == '0000-00-00 00:00:00' ) {
		return;
    }

    $offer = new PhDOffer(["id" => $phd_opening->ID]);
    $config = array(
        'endpoint' => array(
            'localhost' => array(
	        'host' => SOLR_HOST,
            'port' => 8983,
            'path' => '/solr/' . SOLR_PHD_OFFERS_CORE . '/',
            )
        )
    );
    // create a client instance
    $client = new Solarium\Client($config);
    // get an update query instance
    $update = $client->createUpdate();

    // add the documents and a commit command to the update query
    $update->addDocuments(array($offer));
    $update->addCommit();

    // this executes the query and returns the result
    $result = $client->update($update);
}

function delete_phd_opening_solr($phd_opening_guid){
    $config = array(
        'endpoint' => array(
            'localhost' => array(
                'host' => SOLR_HOST,
                'port' => 8983,
                'path' => '/solr/' . SOLR_PHD_OFFERS_CORE . '/',
            )
        )
    );
    // create a client instance
    $client = new Solarium\Client($config);
    // get an update query instance
    $update = $client->createUpdate();

    //create delete document command
    $update->addDeleteById(md5($phd_opening_guid));
    $update->addCommit();

    // this executes the query and returns the result
    $result = $client->update($update);
    echo $result->getStatus();
}

add_action( 'save_post', 'save_phd_opening_solr', 11, 2 );


function save_cooperation_opening_solr( $cooperation_opening_id, $cooperation_opening ){
    if ( $cooperation_opening->post_type != 'cooperation-calls' ) {
		return;
    }
    if ( $cooperation_opening->post_status == 'trash' ) {
        //offers are identified by md5 hash of the guid
        delete_cooperation_opening_solr($cooperation_opening->guid);
        return;
    }
	//skip draft savings.
    if ( $cooperation_opening->post_date_gmt == '0000-00-00 00:00:00' ) {
		return;
    }

    $offer = new CooperationOffer(["id" => $cooperation_opening->ID]);
    $config = array(
        'endpoint' => array(
            'localhost' => array(
	        'host' => SOLR_HOST,
            'port' => 8983,
            'path' => '/solr/' . SOLR_CALLS_CORE . '/',
            )
        )
    );
    // create a client instance
    $client = new Solarium\Client($config);
    // get an update query instance
    $update = $client->createUpdate();

    // add the documents and a commit command to the update query
    $update->addDocuments(array($offer));
    $update->addCommit();

    // this executes the query and returns the result
    $result = $client->update($update);
}

function delete_cooperation_opening_solr($cooperation_opening_guid){
    $config = array(
        'endpoint' => array(
            'localhost' => array(
                'host' => SOLR_HOST,
                'port' => 8983,
                'path' => '/solr/' . SOLR_CALLS_CORE . '/',
            )
        )
    );
    // create a client instance
    $client = new Solarium\Client($config);
    // get an update query instance
    $update = $client->createUpdate();

    //create delete document command
    $update->addDeleteById(md5($cooperation_opening_guid));
    $update->addCommit();

    // this executes the query and returns the result
    $result = $client->update($update);
    echo $result->getStatus();
}

add_action( 'save_post', 'save_cooperation_opening_solr', 11, 2 );