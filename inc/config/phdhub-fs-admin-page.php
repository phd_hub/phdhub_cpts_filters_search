<?php

defined('ABSPATH') or die;

add_action ('admin_menu', 'phdhub_cpts_filters_search_admin_page');

function phdhub_cpts_filters_search_admin_page(){
    add_menu_page ( "PhDHub CPTS Filters and Search Plugin admin page", "PhDHub CPTS Filters and Search Plugin", "manage_options", "phdhub_cpts_filters_search", "phdhub_cpts_filters_search_plugin_admin" );    
}

function phdhub_cpts_filters_search_plugin_admin(){
    echo "<h1>PhD Hub CPTS Filters and Search Plugin Admin</h1>";
    echo "<button onclick='delete_solr()'>Delete all PhD Openings from SOLR</button>";
    echo "<button onclick='reindex_solr()'>Re-index all PhD Openings on SOLR</button>";
    echo "<p id='solr_status'></p>";
}

add_action("wp_ajax_delete_solr_documents", "delete_solr_documents");
add_action("wp_ajax_reindex_solr_documents", "reindex_solr_documents");

//TODO: implement indexing logic
function reindex_solr_documents($blog_id = null){
    $posts = get_posts([
        'post_type' => 'phd-openings',
        'post_status' => 'publish'
    ]);
    foreach($posts as $post){
        save_phd_opening_solr( $post->ID, $post );
    }
}

function delete_solr_documents($blog_id = null){
    $posts = get_posts([
        'post_type' => 'phd-openings',
        'post_status' => 'publish'
    ]);
    foreach($posts as $post){
        delete_phd_opening_solr( $post->guid );
    }
}


add_action( 'admin_enqueue_scripts', 'cpts_filters_enqueue' );
function cpts_filters_enqueue( $hook ) {    
    if( 'toplevel_page_phdhub_cpts_filters_search' != $hook ) return;
        wp_enqueue_script( 'ajax-script',
        plugins_url( PHDHUB_CPTS_SEARCH_PLUGIN_DIR . '/assets/js/admin-script.js' ),
        array( 'jquery' )
    );
}

