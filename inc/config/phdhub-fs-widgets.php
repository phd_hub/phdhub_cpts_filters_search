<?php
/*
 * PhD Hub Filters & Search Widgets
 *
 * Custom widgets for PhD Hub's CPTs
 */

defined('ABSPATH') or die;

/*
 * Create PhD Hub CPTs Filters widget 
 * It displays the filters for the PhD Openings
 */
class phd_openings_filters extends WP_Widget {

	function __construct() {
		parent::__construct(
		'phd_openings_filters', 
		__('PhD Openings Filters', 'phdhub-fs'), 
		array( 'description' => __( 'Display PhD Openings based on these filters', 'phdhub-fs' ), ) 
		);
	}

	/*
	 * The frontend section of the widget
	 */
	public function widget( $widget_args, $instance ) {
		
        extract( $widget_args );
		$style = $instance['style'];
		
		echo $widget_args['before_widget'];
		
		/*
		 * Include style based on the widget's settings
		 */
		if ($style == 'default') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/styles/default.php';
		}
		else if ($style == 'advanced') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/styles/advanced.php';
		}
		else if ($style == 'advanced-calls') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/styles/advanced-calls.php';
		}
		else if ($style == 'advanced-homepage') {
			include dirname(plugin_dir_path(__DIR__)) . '/inc/styles/advanced-homepage.php';
		}
		
		echo $widget_args['after_widget'];
	}

	/*
	 * The backend section of the widget
	 */
	public function form( $instance ) {
		$style =  isset( $instance['style'] ) ? $instance[ 'style' ] : ' ';
?>
	<p>
		<label for="<?php echo $this->get_field_id( 'style' ); ?>"><?php _e( 'Style:', 'phdhub-fs' ); ?></label> 
		<select class="widefat" type="text" id="<?php echo $this->get_field_id( 'style' ); ?>" name="<?php echo $this->get_field_name( 'style' ); ?>">
			<option value="default" <?php echo ($style == 'default')?'selected':''; ?>>Default</option>
			<option value="advanced" <?php echo ($style == 'advanced')?'selected':''; ?>>Advanced</option>
			<option value="advanced-calls" <?php echo ($style == 'advanced-calls')?'selected':''; ?>>Advanced (Calls)</option>
			<option value="advanced-homepage" <?php echo ($style == 'advanced-homepage')?'selected':''; ?>>Advanced (Homepage)</option>
		</select>
	</p>
<?php
	}
	
	/*
	 * Update widget's settings
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = $old_instance;
		$instance['style'] = $new_instance['style'];
		return $instance;
	}

}

/*
 * Register the widgets
 */
function phdhub_fs_widget_registration() {
	register_widget( 'phd_openings_filters' );
}
add_action( 'widgets_init', 'phdhub_fs_widget_registration' );

?>