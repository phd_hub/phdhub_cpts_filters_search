<?php
/*
 * PhD Hub Filters & Search Widgets
 *
 * Widget Style: Default
 */

defined('ABSPATH') or die;
?>
<div class="phdhub-filters">
	<form role="search" method="get" action="<?php echo site_url('/'); ?>">
		<p>
			<input type="text" name="s" placeholder="<?php echo __('Search Offers', 'phdhub-fs'); ?>...">
		</p>
		<p>
			<span><?php echo __('Filters', 'phdhub-fs'); ?></span>
		</p>
		<label><?php echo __('Field of Science', 'phdhub-fs'); ?></label>
		<p class="scrollbar-box">
			<?php 
				$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
				foreach($hiterms as $key => $hiterm) {
			?>
			<span class="checkbox-row">
				<input type="checkbox" name="field_of_science" value="<?php echo $hiterm->term_id; ?>"><?php echo $hiterm->name; ?>
				<?php 
					$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
					foreach($loterms as $key => $loterm) {
				?>
				<span class="checkbox-row">
					<input type="checkbox" class="child" name="field_of_science" value="<?php echo $loterm->term_id; ?>"><?php echo $loterm->name; ?>
				</span>
				<?php 
					}
				?>
			</span>
			<?php
				}
			?>
		</p>
		<label><?php echo __('Region', 'phdhub-fs'); ?></label>
		<p class="scrollbar-box">
			<?php 
				$hiterms = get_terms("regions", array("hide_empty" => false, "parent" => 0));
				foreach($hiterms as $key => $hiterm) {
			?>
			<span class="checkbox-row">
				<input type="checkbox" name="region" value="<?php echo $hiterm->name; ?>"><?php echo $hiterm->name; ?>
				<?php 
					$loterms = get_terms("regions", array("hide_empty" => false, "parent" => $hiterm->term_id));
					foreach($loterms as $key => $loterm) {
				?>
				<span class="checkbox-row">
					<input type="checkbox" class="child" name="region" value="<?php echo $loterm->name; ?>"><?php echo $loterm->name; ?>
				</span>
				<?php 
					}
				?>
			</span>
			<?php 
				}
			?>
		</p>
		<p>
			<label><?php echo __('Master Degree', 'phdhub-fs'); ?></label>
			<select name="master_degree">
				<option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
				<option value="required"><?php echo __('Required', 'phdhub-fs'); ?></option>
				<option value="optional"><?php echo __('Optional', 'phdhub-fs'); ?></option>
			</select>
		</p>
		<p>
			<label><?php echo __('Full/Part Time', 'phdhub-fs'); ?></label>
			<select name="type">
				<option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
				<option value="full-time"><?php echo __('Full Time', 'phdhub-fs'); ?></option>
				<option value="part-time"><?php echo __('Part Time', 'phdhub-fs'); ?></option>
			</select>
		</p>
		<p>
			<label><?php echo __('Funded', 'phdhub-fs'); ?></label>
			<select name="funded">
				<option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
				<option value="partial"><?php echo __('Partial Funded', 'phdhub-fs'); ?></option>
				<option value="full"><?php echo __('Full Funded', 'phdhub-fs'); ?></option>
				<option value="no"><?php echo __('Not Funded', 'phdhub-fs'); ?></option>
			</select>
		</p>
		<p>
			<input type="submit" name="find_phd_offers" value="<?php echo __('Find Offers', 'phdhub-fs'); ?>">
	</form>
</div>