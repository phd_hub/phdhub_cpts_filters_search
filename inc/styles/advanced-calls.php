<?php
/*
 * PhD Hub Filters & Search Widgets
 *
 * Widget Style: Default
 */

defined('ABSPATH') or die;
?>
<div class="phdhub-advanced-filters">
	<form role="search" method="get" action="<?php echo site_url('/'); ?>">
		<div class="uk-grid">
			<div class="uk-width-1-2">
				<p>
					<label>
						<span class="uk-hidden"><?php echo __('Search Offers'); ?></span>
						<input type="text" name="s" placeholder="<?php echo __('Search Offers', 'phdhub-fs'); ?>..." required>
					</label>
					<input type="hidden" name="post_type" value="cooperation-calls" />
					<span class="search-offers-btn">
						<input type="submit" name="find_cooperation_offers" value="<?php echo __(' ', 'phdhub-fs'); ?>">
					</span>
				</p>
			</div>
			<div class="uk-width-1-2">
				<p class="advanced-link">
					<a href="#advanced-search-form" uk-toggle><?php echo __('Use Advanced Search', 'phdhub-fs'); ?></a>
				</p>
			</div>
		</div>
	</form>
		
	<div id="advanced-search-form" class="uk-modal-full" uk-modal>
		<div class="uk-modal-dialog">
			<div class="fos-container">
				<h4><?php echo __('Advanced Search', 'phdhub-fs'); ?></h4>
				<button class="uk-modal-close-full uk-close-large" type="button" uk-close><span class="uk-hidden"><?php echo __('Close', 'phdhub-fs'); ?></span></button>
				<form role="search" method="get" action="<?php echo site_url('/'); ?>">
					<div class="uk-grid">
						<div class="uk-width-1-1">
							<p>
								<label>
									<span class="uk-hidden"><?php echo __('Add keywords or keyphrases here', 'phdhub-fs'); ?></span>    
									<input type="text" name="s" placeholder="<?php echo __('Add keywords or keyphrases here', 'phdhub-fs'); ?>...">
								</label>
							</p>
						</div>
						<div class="uk-width-1-2">
							<p>
								<label><?php echo __('Fields of Science', 'phdhub-fs'); ?></label>
								<select name="field_of_science[]" class="fields-of-science-multiple" multiple="multiple">
									<?php 
										$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
										foreach($hiterms as $key => $hiterm) {
									?> 
									<option value="<?php echo $hiterm->term_id; ?>"><?php echo $hiterm->name; ?></option>
									<?php
											$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
											foreach($loterms as $key => $loterm) {
									?>
									<option value="<?php echo $loterm->term_id; ?>"><?php echo $loterm->name; ?></option>
                                    <?php
											}
										}
									?>
								</select>
							</p>
						</div>
						<div class="uk-width-1-2">
							<p>
								<label><?php echo __('Regions', 'phdhub-fs'); ?></label>
							</p>
							<div class="uk-panel uk-panel-scrollable">
								<?php 
									$hiterms = get_terms("regions", array("hide_empty" => false, "parent" => 0));
									foreach($hiterms as $key => $hiterm) {
								?>
								<p>
									<label>
										<input type="checkbox" name="region" value="<?php echo $hiterm->name; ?>"><?php echo $hiterm->name; ?>
									</label>
									<?php 
										$loterms = get_terms("regions", array("hide_empty" => false, "parent" => $hiterm->term_id));
										foreach($loterms as $key => $loterm) {
									?>
									<span class="regions-child">
										<label>
											<input type="checkbox" class="child" name="region" value="<?php echo $loterm->name; ?>"><?php echo $loterm->name; ?>
										</label>
									</span>
									<?php 
										}
									?>
								</p>
								<?php 
									}
								?>
							</div>
						</div>
					</div>

					<p>
						<input type="hidden" name="post_type" value="cooperation-calls" />
						<input type="submit" name="find_cooperation_offers_advanced" value="<?php echo __('Search', 'phdhub-fs'); ?>">
					</p>
				</form>
			</div>
		</div>
	</div>
</div>