<?php
/*
 * PhD Hub Filters & Search Widgets
 *
 * Widget Style: Default
 */

defined('ABSPATH') or die;
?>
<div class="phdhub-advanced-filters-home">
	<form role="search" method="get" action="<?php echo site_url('/'); ?>">
		<p>
			<label for="simple-search-field" class="uk-hidden"><?php echo __('Search for a field, organization, region', 'phdhub-fs'); ?></label>
			<input type="text" name="s" id="simple-search-field" placeholder="<?php echo __('Search for a field, organization, region', 'phdhub-fs'); ?>..." required>
			<input type="hidden" name="post_type" value="phd-openings" />
			<input type="submit" name="find_phd_offers" value="<?php echo __('Start', 'phdhub-fs'); ?>">
		</p>
		<p class="advanced-link">
			<a href="#advanced-search-form" uk-toggle><?php echo __('Advanced Search', 'phdhub-fs'); ?></a>
		</p>
	</form>
		
	<div id="advanced-search-form" class="uk-modal-full" uk-modal>
		<div class="uk-modal-dialog">
			<div class="fos-container">
				<h4><?php echo __('Advanced Search', 'phdhub-fs'); ?></h4>
				<button class="uk-modal-close-full uk-close-large" type="button" uk-close><span class="uk-hidden"><?php echo __('Close', 'phdhub-fs'); ?></span></button>
				<form role="search" method="get" action="<?php echo site_url('/'); ?>">
					<div class="uk-grid">
						<div class="uk-width-1-1">
							<p>
                                <label>
                                    <span class="uk-hidden"><?php echo __('Add keywords or keyphrases here', 'phdhub-fs'); ?></span>
								    <input type="text" name="s" placeholder="<?php echo __('Add keywords or keyphrases here', 'phdhub-fs'); ?>...">
                                </label>
							</p>
						</div>
						<div class="uk-width-1-3">
							<p>
								<label><?php echo __('Fields of Science', 'phdhub-fs'); ?></label>
								<select name="field_of_science[]" class="fields-of-science-multiple" multiple="multiple">
									<?php 
										$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
										foreach($hiterms as $key => $hiterm) {
									?> 
									<option value="<?php echo $hiterm->term_id; ?>"><?php echo $hiterm->name; ?></option>
									<?php
											$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
											foreach($loterms as $key => $loterm) {
									?>
									<option value="<?php echo $loterm->term_id; ?>"><?php echo $loterm->name; ?></option>
                                    <?php
											}
										}
									?>
								</select>
							</p>
							<p>
								<label><?php echo __('Regions', 'phdhub-fs'); ?></label>
							</p>
							<div class="uk-panel uk-panel-scrollable">
								<?php 
									$hiterms = get_terms("regions", array("hide_empty" => false, "parent" => 0));
									foreach($hiterms as $key => $hiterm) {
								?>
								<p>
                                    <label>
									   <input type="checkbox" name="region" value="<?php echo $hiterm->name; ?>"><?php echo $hiterm->name; ?>
                                    </label>
									<?php 
										$loterms = get_terms("regions", array("hide_empty" => false, "parent" => $hiterm->term_id));
										foreach($loterms as $key => $loterm) {
									?>
									<span class="regions-child">
                                        <label>
										  <input type="checkbox" class="child" name="region" value="<?php echo $loterm->name; ?>"><?php echo $loterm->name; ?>
                                        </label>
									</span>
									<?php 
										}
									?>
								</p>
								<?php 
									}
								?>
							</div>
						</div>
						<div class="uk-width-1-3">
							<p>
								<?php
									$durations = array();
									$blog_ids = get_sites();
									foreach ($blog_ids as $key=>$current_blog) {
										// switch to each blog to get the posts
										switch_to_blog($current_blog->blog_id);
											$duration_values = get_posts(
												array(
													'post_type' => 'phd-openings',
													'meta_key' => 'duration',
													'posts_per_page' => -1,
												)
											);
											foreach ( $duration_values as $duration_value ) {
												$durations[] = esc_attr( get_post_meta( $duration_value->ID, 'duration', true ) );
											}
										restore_current_blog();
									}
									$minDuration = 0;
									$maxDuration = 0;
									if (!empty ($durations)) {
										$maxDuration = max($durations);
									}
								?>
								<script>
									jQuery(document).ready(
										function($) {
											jQuery( "#slider-range-duration" ).slider({
												range: true,
												min: 0,
												max: <?php echo $maxDuration; ?>,
												values: [ <?php echo $minDuration; ?>, <?php echo $maxDuration; ?> ],
												slide: function( event, ui ) {
													$( "#duration" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
													$( "#duration-min" ).val( ui.values[ 0 ] );
													$( "#duration-max" ).val( ui.values[ 1 ] );
												}
											});
											$( "#duration" ).val( "" + $( "#slider-range-duration" ).slider( "values", 0 ) +
												" - " + $( "#slider-range-duration" ).slider( "values", 1 ) );
											$( "#duration-min" ).val( $( "#slider-range-duration" ).slider( "values", 0 ) );
											$( "#duration-max" ).val( $( "#slider-range-duration" ).slider( "values", 1 ) );
										}
									);
								</script>
								<label>
                                    <?php echo __('Duration (in months)', 'phdhub-cpts'); ?>
								    <input type="text" id="duration" readonly />
                                </label>
								<input type="hidden" id="duration-min" name="duration_min" readonly />
								<input type="hidden" id="duration-max" name="duration_max" readonly />
							</p>
							<div id="slider-range-duration"></div>
							
							<p class="advanced-search-select">
								<label>
                                    <?php echo __('Master Degree', 'phdhub-fs'); ?>
                                    <select name="master_degree">
                                        <option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
                                        <option value="all"><?php echo __('All', 'phdhub-fs'); ?></option>
                                        <option value="required"><?php echo __('Required', 'phdhub-fs'); ?></option>
                                        <option value="optional"><?php echo __('Optional', 'phdhub-fs'); ?></option>
                                    </select>
                                </label>
							</p>
							
							<p class="advanced-search-select">
								<label>
                                    <?php echo __('Full/Part Time', 'phdhub-fs'); ?>
                                    <select name="type">
                                        <option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
                                        <option value="all"><?php echo __('All', 'phdhub-fs'); ?></option>
                                        <option value="full-time"><?php echo __('Full Time', 'phdhub-fs'); ?></option>
                                        <option value="part-time"><?php echo __('Part Time', 'phdhub-fs'); ?></option>
                                    </select>
                                </label>
							</p>
							<p class="advanced-search-select">
								<label>
                                    <?php echo __('Funded', 'phdhub-fs'); ?>
                                    <select name="funded">
                                        <option value="all"><?php echo __('Select', 'phdhub-fs'); ?></option>
                                        <option value="all"><?php echo __('All', 'phdhub-fs'); ?></option>
                                        <option value="partial"><?php echo __('Partial Funded', 'phdhub-fs'); ?></option>
                                        <option value="full"><?php echo __('Full Funded', 'phdhub-fs'); ?></option>
                                        <option value="no"><?php echo __('Not Funded', 'phdhub-fs'); ?></option>
                                    </select>
                                </label>
							</p>
						</div>
						<div class="uk-width-1-3">
							<p>
								<?php
									$tuitions = array();
									$blog_ids = get_sites();
									foreach ($blog_ids as $key=>$current_blog) {
										// switch to each blog to get the posts
										switch_to_blog($current_blog->blog_id);
											$tuition_values = get_posts(
												array(
													'post_type' => 'phd-openings',
													'meta_key' => 'tuition_fees',
													'posts_per_page' => -1,
												)
											);
											foreach ( $tuition_values as $tuition_value ) {
												$tuitions[] = esc_attr( get_post_meta( $tuition_value->ID, 'tuition_fees', true ) );
											}
										restore_current_blog();
									}
									$minTuition = 0;
									$maxTuition = 0;
									if (!empty ($tuitions)) {
										$maxTuition = max($tuitions);
									}
								?>
								<script>
									jQuery(document).ready(
										function($) {
											jQuery( "#slider-range-tuitions" ).slider({
												range: true,
												min: 0,
												max: <?php echo $maxTuition?>,
												values: [ <?php echo $minTuition; ?>, <?php echo $maxTuition; ?> ],
												slide: function( event, ui ) {
													$( "#tuition-fees" ).val( ui.values[ 0 ] + " - " + ui.values[ 1 ] );
													$( "#tuition-fees-min" ).val( ui.values[ 0 ] );
													$( "#tuition-fees-max" ).val( ui.values[ 1 ] );
												}
											});
											$( "#tuition-fees" ).val( "" + $( "#slider-range-tuitions" ).slider( "values", 0 ) +
												" - " + $( "#slider-range-tuitions" ).slider( "values", 1 ) );
											$( "#tuition-fees-min" ).val( $( "#slider-range-tuitions" ).slider( "values", 0 ) );
											$( "#tuition-fees-max" ).val( $( "#slider-range-tuitions" ).slider( "values", 1 ) );
										}
									);
								</script>
								<label>
                                    <?php echo __('Tuition Fees', 'phdhub-cpts'); ?>
								    <input type="text" id="tuition-fees" readonly />
                                </label>
								<input type="hidden" id="tuition-fees-min" name="tuition_fees_min" readonly />
								<input type="hidden" id="tuition-fees-max" name="tuition_fees_max" readonly />
							</p>
							<div id="slider-range-tuitions"></div>
							
							<p class="advanced-search-select">
								<label>
                                    <?php echo __('Currency', 'phdhub-cpts'); ?>
                                    <select name="tuition_fees_currency">
                                        <option value="eur"><?php echo '€'; ?></option>
                                        <option value="bgn"><?php echo 'лв'; ?></option>
                                        <option value="gpb"><?php echo '£'; ?></option>
                                        <option value="hrk"><?php echo 'kn'; ?></option>
                                        <option value="czk"><?php echo 'Kč'; ?></option>
                                        <option value="dkk"><?php echo 'kr'; ?></option>
                                        <option value="huf"><?php echo 'Ft'; ?></option>
                                        <option value="pln"><?php echo 'zł'; ?></option>
                                        <option value="ron"><?php echo 'Leu'; ?></option>
                                        <option value="chf"><?php echo 'Fr.'; ?></option>
                                    </select>
                                </label>
							</p>
							
							<p>
								<label>
                                    <?php echo __('Starting Date', 'phdhub-cpts'); ?>
								    <input type="date" name="starting_date" />
                                </label>
							</p>
							
							<p>
								<label>
                                    <?php echo __('Deadline to Apply', 'phdhub-cpts'); ?>
								    <input type="date" name="deadline" />
                                </label>
							</p>
						</div>
					</div>

					<p>
						<input type="hidden" name="post_type" value="phd-openings" />
						<input type="submit" name="find_phd_offers_advanced" value="<?php echo __('Search', 'phdhub-fs'); ?>">
					</p>
				</form>
			</div>
		</div>
	</div>
</div>
