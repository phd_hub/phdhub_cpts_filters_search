<?php

use Solarium\QueryType\Select\Result\Document;

class CooperationOfferPosting {
    
    private $document;
    
    public function __construct(Document $document) {
        $this->document = $document;
        $this->title = $this->setTitle();
        $this->short = $this->setShortInfo();
        $this->permalink = $this->setPermalink();
        $this->institution_permalink = $this->setInstitutionPermalink();
        $this->institution_label = $this->setInstitutionLabel();
        $this->institution_logo = $this->setInstitutionLogo();
        $this->institution_city = $this->setInstitutionCity();
        $this->institution_country = $this->setInstitutionCountry();
        $this->categories_labels = $this->setCategoriesLabels();
        $this->categories_permalink = $this->setCategoriesPermalinks();
        $this->post_month = $this->setPostMonth();
        $this->post_day = $this->setPostDay();
		
// --------> New code --------		

		$this->company_permalink = $this->setCompanyPermalink();
        $this->company_label = $this->setCompanyLabel();
        $this->company_logo = $this->setCompanyLogo();
        $this->company_city = $this->setCompanyCity();
        $this->company_country = $this->setCompanyCountry();
		
		// xreiazontai ?
		// efoson nai, prepei na mpoun kai sto search-cooperation-offers (lines 133-136) ta parakatw: 
		/*
			<p class="description"><?php echo $offer->call_info; ?></p>
		<h4>
			<?php echo $offer->email_address; ?>
		</h4>
		*/
				
//		$this->call_info = $this->setCallInfo();
//		$this->email_address = $this->setEmailAddress();

// <-------- New Code --------
    }
    
    private function setTitle(){
	    return $this->document->post_title[0];
    }
    
    private function setShortInfo(){
	    return substr(wp_strip_all_tags($this->document->cooperation_opening_info[0]), 0, 195) . "...";
    }
	
    private function setPermalink(){
	    return $this->document->post_permalink[0];
    }
    
    private function setInstitutionPermalink(){
	    return $this->document->institution_permalink[0];
    }
    
    private function setInstitutionLogo(){
	    return $this->document->institution_logo[0];
    }
    
    private function setInstitutionLabel(){
	    return $this->document->institution_label[0];
    }
    
    private function setCategoriesPermalinks(){	
	    return $this->document->categories_permalink;
    }
    
    private function setCategoriesLabels(){
	    return $this->document->categories_name;
    }
    
    private function setPostMonth(){
	    return mysql2date("M", $this->document->post_date[0], true);
    }
    
    private function setPostDay(){
	    return mysql2date("d", $this->document->post_date[0], true);
    }
    
    private function setInstitutionCity(){
	    return $this->document->institution_city[0];
    }
    
    private function setInstitutionCountry(){
	    return $this->document->institution_country[0];
    }

// ---------> New Code --------

	private function setCompanyPermalink(){
	    return $this->document->company_permalink[0];
    }
    
    private function setCompanyLogo(){
	    return $this->document->company_logo[0];
    }
    
    private function setCompanyLabel(){
	    return $this->document->company_label[0];
    }
	
	private function setCompanyCity(){
	    return $this->document->company_city[0];
    }
    
    private function setCompanyCountry(){
	    return $this->document->company_country[0];
    }
	
	// xreiazetai ?
	// table name ?
	// length 195 ?
//	private function setCallInfo(){
//	    return substr(wp_strip_all_tags($this->document->cooperation_call_info[0]), 0, 195) . "...";
 //   }
	
	// xreiazetai?
//	private function setEmailAddress(){
//	    return $this->document->email_address;
 //   }
    
// <-------- New Code --------
}
