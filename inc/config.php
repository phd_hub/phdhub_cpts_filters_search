<?php
/*
 * PhD Hub CPTs Filters & Search - Files
 * Include all required files
 */
defined('ABSPATH') or die;

/*
 * Core Files
 */
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-fs-functions.php'; // Custom Functions
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-fs-widgets.php'; // Register Plugin's Widgets
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-fs-scripts.php'; // Load CSS/JS Scripts
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-fs-admin-page.php'; // Load CSS/JS Scripts

?>